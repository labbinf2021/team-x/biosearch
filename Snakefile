import os
from webbrowser import get


API_KEY = os.environ.get("API_KEY")
if API_KEY == None:
    API_KEY = ""
if os.environ.get("SPECIES"):
    SPECIES = os.environ.get("SPECIES")
else:
    SPECIES = "Passer domesticus"


SPECIES_DIR = "_".join(SPECIES.split())


if os.environ.get("TAXONOMY_LEVEL"):
    TAXONOMY_LEVEL = os.environ.get("TAXONOMY_LEVEL")
else:
    TAXONOMY_LEVEL = "family"

if os.environ.get("NGEN"):
    NGEN = os.environ.get("NGEN")
else:    
    NGEN = "20000"


rule all:
    input:
        expand("{sd}/trees", sd = SPECIES_DIR)

rule ncbi_go:
    params:
        sp = SPECIES,
        tl = TAXONOMY_LEVEL,
        ak = API_KEY
    output:
        directory(expand("{sd}/genes/", sd = SPECIES_DIR))
    shell:
        "python3 ncbi_downloader.py {params.sp:q} {params.tl:q} {params.ak}"

rule align:
    params:
        sd = SPECIES_DIR,
        sp = SPECIES
    input:
        rules.ncbi_go.output
    output:
        directory(expand("{sd}/ordered_genes/", sd = SPECIES_DIR))
    shell:
        """rm {params.sd}/genes/.snakemake_timestamp
        python3 mafft.py {params.sp:q}"""        

rule supertree:
    params:
        sd = SPECIES_DIR
    input:
        rules.align.output
    output:
        directory(expand("{sd}/super_aln_genes/", sd = SPECIES_DIR))
    shell:
        "python3 concatenate_aln_seqs.py {params.sd}"    

rule mrbayes:
    params:
        sd = SPECIES_DIR,
        ng = NGEN
    input:
        rules.align.output,
        rules.supertree.output
    output:
        directory(expand("{sd}/mb/", sd = SPECIES_DIR))
    shell:
        "bash mrbayes.sh {params.sd} {params.ng}"

rule drawtree:
    params:
        sd = SPECIES_DIR
    input:
        rules.mrbayes.output
    output:
        directory(expand("{sd}/trees", sd = SPECIES_DIR))
    shell:
        "python3 tree.py {params.sd}"
