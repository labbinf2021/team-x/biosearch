# BioSearch

BioSearch is a project within the dicipline of bioinformathic labs made by 4 students. 

This program will receive your inputs and return a super phylogenetic tree ready to be analyzed!

## Dependencies

To use Biosearch you need only to install, the rest of the dependencies will be installed when you download the Docker image. You can see how to install Docker [here](https://docs.docker.com/get-docker/).

## Getting started
### Download code
To download the code via clone make sure you include the git submodules:
```
git clone --recurse-submodules https://gitlab.com/labbinf2021/team-x/biosearch.git
```
### Download image
If you don't have Docker installed do it.  
If you have Docker already installed, do the download of the image to execute the script. To do that execute:
```
docker pull cr201900225/biosearch:latest
```  
### Execute Biosearch
To create, start a container and execute the program use the generic command:  
```
docker run -v /home/USER_NAME/OUTPUT/PATH/SPECIES_NAME:/biosearch/SPECIES_NAME [-e "ENVIRONMENT_VARIABLE_NAME=ENVIRONMENT VARIABLE VALUE"] -i -t cr201900225/biosearch /opt/conda/envs/snakemake/bin/snakemake --cores all
```
-e argument can be used how many times you want.  

And done! All the trees are outputed to ```/home/USER_NAME/OUTPUT/PATH/SPECIES_NAME/trees``` dir.

## Environment variables

There are some environment variables that you can pass do Docker when you create the image.

**SPECIES** - The species name you want the program searchs for. DEFAULT - "Passer domesticus".

**TAXONOMY_LEVEL** - The taxonomy level which complements the species specifically. DEFAULT - "family".  
Can be:
- superkingdom;
- kingdom;
- phylum;
- subphylum;
- superclass
- class;
- infraclass;
- superorder
- order;
- suborder
- infraorder;
- superfamily;
- family;
- genus.

Some of the values may be not available for all the species.

**API_KEY** - API key of [NCBI](https://www.ncbi.nlm.nih.gov/). DEFAULT - ""

**NGEN** - Number of generation given to Mrbayes. DEFAULT - "20000"

The environment variable names must be in **UPPER CASE**.

## Examples

Some examples how to run Biosearch after getting the image:  

```
docker run -v "/home/user1/Documents/Passer_domesticus:/biosearch/Passer_domesticus" -e "API_KEY=6f813a1ecf689c068291940650dd8e95c476" -i -t cr201900225/biosearch /opt/conda/envs/snakemake/bin/snakemake --cores all
```

```
docker run -v "/home/user2/Documents/Psammodromus_algirus:/biosearch/Psammodromus_algirus" -e "API_KEY=6f813a1ecf689c068291940650dd8e95c476" -e "SPECIES=Psammodromus algirus" -i -t cr201900225/biosearch /opt/conda/envs/snakemake/bin/snakemake --cores all
```

```
docker run -v "/home/user3/Documents/species/Giraffa_camelopardalis:/biosearch/Giraffa_camelopardalis" -e "SPECIES=Giraffa camelopardalis" -e "TAXONOMY_LEVEL=infraorder" -e "API_KEY=6f813a1ecf689c068291940650dd8e95c476" -i -t cr201900225/biosearch /opt/conda/envs/snakemake/bin/snakemake --cores all
```

This **API_KEY** doesn't really exists, use your instead or ignores it.
