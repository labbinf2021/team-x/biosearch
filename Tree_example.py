#!/usr/bin/env python
# coding: utf-8



import toytree


# ### Exemplos-




tre = toytree.tree("https://eaton-lab.org/data/Cyathophora.tre")






rtre = tre.root(wildcard='prz')
rtre.draw(width=400, tip_labels_align=True);

tre.root(wildcard='prz').drop_tips(wildcard="tham").ladderize().draw();

rtre.draw(
    tip_labels_colors='pink',
    node_labels='support',
    node_sizes=15,
    node_colors="cyan",
    edge_style={
        "stroke": "darkgrey", 
        "stroke-width": 3,
    },
)


# ### DADOS OBTIDOS EM ASB




tre = toytree.tree("example.con.tre", tree_format =10)





# root the tree using a wildcard string matching and draw a tree figure.
tre.draw(width=1000, tip_labels_align=True);





# draw the tree using bootstrap as an option.
tre.draw(
    tree_style='d',
    width = 1000,
    tip_labels_colors='pink',
    node_labels='support',
    node_sizes=15,
    node_colors="cyan",
    edge_style={
        "stroke": "darkgrey", 
        "stroke-width": 2,
    },
)




tree = tre.draw(
    width = 1000,
    tip_labels_colors='green',
    node_sizes=15,
    node_colors="cyan",
    edge_style={
        "stroke": "darkgrey", 
        "stroke-width": 2,
    },
)





# show all the bootstrap's values 
tre.get_node_values("support", show_root=1, show_tips=1)





# Show all the values from the 'prob' variable 
nodes = tre.get_node_values("prob", show_root=0, show_tips=0)
nodes = [0 if node == "" else int(float(node)*100) for node in nodes]

print(nodes)





# draw the tree using bootstrap witch had obtained from the previous code 'nodes'.
tre.draw(
    width = 1000,
    tip_labels_colors='green',
    node_labels=nodes,
    node_sizes=10,
    node_colors="cyan",
    edge_style={
        "stroke": "darkgrey", 
        "stroke-width": 2,
    },
)

