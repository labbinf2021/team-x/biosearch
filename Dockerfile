#
# Biosearch Dockerfile
#

# Pull base image.
FROM snakemake/snakemake:latest

RUN apt-get update
RUN apt-get install mafft -y
RUN apt-get install mrbayes -y


#We used Pull Command to work snakemake through docker
#following command used in terminal:
RUN pip3 install beautifulsoup4==4.10.0
RUN pip3 install requests==2.25.1
RUN pip3 install toyplot==0.19.0
RUN pip3 install numpy==1.20.1
RUN pip3 install toytree==2.0.1
RUN pip3 install simplejson==3.16.0

WORKDIR /biosearch

COPY Snakefile /biosearch
COPY ncbi_downloader.py /biosearch
COPY mafft.py /biosearch
COPY mrbayes.sh /biosearch
COPY tree.py /biosearch
COPY concatenate_aln_seqs.py /biosearch
COPY nexus_converter/ /biosearch/nexus_converter/
