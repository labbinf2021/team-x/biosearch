#!/usr/bin/env bash


function exec_mb(){
	# this function reads a species dir and a bootstrap value, transform the aligned fasta files in
	# nexus files with a Mrbayes block and performs a Mrbayes analyses with multiprocessing (forks).
	# species_dir -> species dir created by ncbi_downloader with data already aligned
	# bootstrap   -> bootstrap value for the Mrbayes block on nexus file

	species_dir=$1
	bootstrap=$2
	mkdir $species_dir/mb
	files=$(ls $species_dir/ordered_genes/)
	
	for file in $files
	do
		./nexus_converter/bio_converter.py $species_dir/ordered_genes/$file $bootstrap
	done
	
	nex_files=$(ls $species_dir/ordered_genes/*nex) # outputs directly the files with path
	
	for nex_file in $nex_files
	do
		# Changes the default filename (ordered_genes) to mb
		sed -i "s/ordered_genes/mb/g" $nex_file 
		mb $nex_file & # "&" means execute in a new process
	done
	wait # wait for all processes to finish (forks)


	file=$(ls $species_dir/super_aln_genes/all_super_species.fasta)
	./nexus_converter/bio_converter.py $file $bootstrap
	
	nex_file=$(ls $species_dir/super_aln_genes/all_super_species.nex) # outputs directly the files with path

	# Changes the default filename (super_aln_genes) to mb
	sed -i "s/super_aln_genes/mb/g" $nex_file 
	mb $nex_file
}


# exec_mb species_dir bootstrap
# example:
# exec_mb Passer_domesticus 200000

exec_mb "$1" "$2"
