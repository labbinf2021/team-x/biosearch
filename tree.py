# -*- coding: utf-8 -*-

from sys import argv
import toytree
import toyplot.svg
from os import listdir, mkdir
from os.path import isdir


def main(species):
    path = f"{species}/mb"
    
    tree_files = listdir(f"{path}")
    tree_files = [tree_file for tree_file in tree_files if tree_file.endswith(".con.tre")]
        
    if not isdir(f"{species}/trees"):
        mkdir(f"{species}/trees")
    
    for tree_file in tree_files:
            
        tre = toytree.tree(f"{path}/{tree_file}", tree_format=10)
        
        # root the tree using a wildcard string matching and draw a tree figure.
        tre.draw(width=1000, tip_labels_align=True);
        
        # Show all the values from the 'prob' variable 
        nodes = tre.get_node_values("prob", show_root=0, show_tips=0)
        nodes = ["" if node == "" else int(float(node)*100) for node in nodes]
        
        # draw the tree using bootstrap witch had obtained from the previous code 'nodes'.
        canvas, axes, mark = tre.draw(
            width=1000,
            tip_labels_colors='green',
            node_labels=nodes,
            node_sizes=15,
            node_colors="cyan",
            edge_style={
                "stroke": "darkgrey", 
                "stroke-width": 2,
            },
        )

        # writes the trees to a file
        tree_file = tree_file.split(".")[0]
        toyplot.svg.render(canvas, f"{species}/trees/{tree_file}.svg")
    
if __name__ == '__main__':
    main(argv[1])
    
